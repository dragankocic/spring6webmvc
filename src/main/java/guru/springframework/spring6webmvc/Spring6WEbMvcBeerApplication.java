package guru.springframework.spring6webmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring6WEbMvcBeerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Spring6WEbMvcBeerApplication.class, args);
    }

}
