package guru.springframework.spring6webmvc.bootstrap;

import guru.springframework.spring6webmvc.entities.Beer;
import guru.springframework.spring6webmvc.entities.Customer;
import guru.springframework.spring6webmvc.model.BeerStyle;
import guru.springframework.spring6webmvc.repositories.BeerRepository;
import guru.springframework.spring6webmvc.repositories.CustomerRepository;
import guru.springframework.spring6webmvc.service.BeerCsvService;
import guru.springframework.spring6webmvc.service.BeerCsvServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class BootstrapData implements CommandLineRunner {

    private final BeerRepository beerRepository;
    private final CustomerRepository customerRepository;
    private final BeerCsvService beerCsvService;

    @Transactional
    @Override
    public void run(String... args) throws Exception {
        loadBeerData();
        loadCsvData();
        loadCustomerData();
    }
    private void loadCsvData() throws FileNotFoundException {
        if (beerRepository.count() < 10) {
            var file = ResourceUtils.getFile("classpath:csvdata/beers.csv");

            var recs = beerCsvService.convertCSV(file);

            recs.forEach(beerCSVRecord -> {
                BeerStyle beerStyle = switch (beerCSVRecord.getStyle()) {
                    case "American Pale Lager" -> BeerStyle.LAGER;
                    case "American Pale Ale (APA)", "American Black Ale", "Belgian Dark Ale", "American Blonde Ale" ->
                            BeerStyle.ALE;
                    case "American IPA", "American Double / Imperial IPA", "Belgian IPA" -> BeerStyle.IPA;
                    case "American Porter" -> BeerStyle.PORTER;
                    case "Oatmeal Stout", "American Stout" -> BeerStyle.STOUT;
                    case "Saison / Farmhouse Ale" -> BeerStyle.SAISON;
                    case "Fruit / Vegetable Beer", "Winter Warmer", "Berliner Weissbier" -> BeerStyle.WHEAT;
                    case "English Pale Ale" -> BeerStyle.PALE_ALE;
                    default -> BeerStyle.PILSNER;
                };

                beerRepository.save(Beer.builder()
                        .name(StringUtils.abbreviate(beerCSVRecord.getBeer(), 50))
                        .style(beerStyle)
                        .price(BigDecimal.TEN)
                        .upc(beerCSVRecord.getRow().toString())
                        .quantityOnHand(beerCSVRecord.getCount())
                        .build());
            });
        }
    }

    private void loadBeerData() {
        if (beerRepository.count() > 0) {
            return;
        }

        var beer1 = Beer.builder()
                .name("Galaxy Cat")
                .style(BeerStyle.PALE_ALE)
                .upc("123456")
                .price(new BigDecimal("12.99"))
                .quantityOnHand(122)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var beer2 = Beer.builder()
                .name("Crank")
                .style(BeerStyle.PALE_ALE)
                .upc("12356222")
                .price(new BigDecimal("11.99"))
                .quantityOnHand(392)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var beer3 = Beer.builder()
                .name("Sunshine City")
                .style(BeerStyle.IPA)
                .upc("12356")
                .price(new BigDecimal("13.99"))
                .quantityOnHand(144)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        log.info("Seeding beers");
        beerRepository.saveAll(List.of(beer1, beer2, beer3));
    }

    private void loadCustomerData() {
        if (customerRepository.count() > 0) {
            return;
        }

        var customer1 = Customer.builder()
                .name("Albert Einstein")
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var customer2 = Customer.builder()
                .name("Isaak Newton")
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var customer3 = Customer.builder()
                .name("Stephen Hawkins")
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();
        log.info("Seeding customers");
        customerRepository.saveAll(List.of(customer1, customer2, customer3));
    }
}
