package guru.springframework.spring6webmvc.model;

public enum BeerStyle {
    LAGER, PILSNER, STOUT, GOSE, PORTER, ALE, WHEAT, IPA, PALE_ALE, SAISON
}
