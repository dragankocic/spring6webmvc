package guru.springframework.spring6webmvc.mappers;

import guru.springframework.spring6webmvc.entities.Customer;
import guru.springframework.spring6webmvc.model.CustomerDTO;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {

    Customer customerDtoToCustomer(CustomerDTO customerDto);
    CustomerDTO customerToCustomerDto(Customer customer);
}
