package guru.springframework.spring6webmvc.mappers;

import guru.springframework.spring6webmvc.entities.Beer;
import guru.springframework.spring6webmvc.model.BeerDTO;
import org.mapstruct.Mapper;

@Mapper
public interface BeerMapper {

    Beer beerDtoToBeer(BeerDTO beerDto);

    BeerDTO beerToBeerDto(Beer beer);
}
