package guru.springframework.spring6webmvc.controller;

import guru.springframework.spring6webmvc.model.CustomerDTO;
import guru.springframework.spring6webmvc.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
@RestController
public class CustomerController {
    public static final String CUSTOMER_PATH = "/api/v1/customer";
    public static final String CUSTOMER_PATH_ID = CUSTOMER_PATH + "/{id}";

    private final CustomerService customerService;

    @PostMapping(CUSTOMER_PATH)
    public ResponseEntity save(@RequestBody CustomerDTO customerDTO) {
        var savedCustomer = customerService.save(customerDTO);

        var headers = new HttpHeaders();
        headers.add("location", "/api/v1/customer/" + savedCustomer.getId());

        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @GetMapping(CUSTOMER_PATH)
    public List<CustomerDTO> get() {
        return customerService.get();
    }

    @GetMapping(CUSTOMER_PATH_ID)
    public CustomerDTO getById(@PathVariable("id") UUID id) {
        log.debug("Get Customer by ID - in controller");

        return customerService.getById(id).orElseThrow(NotFoundException::new);
    }

    @PutMapping(CUSTOMER_PATH_ID)
    public ResponseEntity update(@PathVariable("id") UUID id, @RequestBody CustomerDTO customerDTO) {
        if (customerService.update(id, customerDTO).isEmpty()) {
            throw new NotFoundException();
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(CUSTOMER_PATH_ID)
    public ResponseEntity patch(@PathVariable("id") UUID id, @RequestBody CustomerDTO customerDTO) {
        customerService.patch(id, customerDTO);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(CUSTOMER_PATH_ID)
    public ResponseEntity delete(@PathVariable("id") UUID id) {
        if(!customerService.delete(id)) {
            throw new NotFoundException();
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
