package guru.springframework.spring6webmvc.controller;

import guru.springframework.spring6webmvc.model.BeerDTO;
import guru.springframework.spring6webmvc.model.BeerStyle;
import guru.springframework.spring6webmvc.service.BeerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@RestController
public class BeerController {
    public static final String BEER_PATH = "/api/v1/beer";
    public static final String BEER_PATH_ID = BEER_PATH + "/{id}";

    private final BeerService beerService;

    @PostMapping(BEER_PATH)
    public ResponseEntity save(@Validated @RequestBody BeerDTO beerDTO) {
        var savedBeer = beerService.save(beerDTO);

        var headers = new HttpHeaders();
        headers.add("location", "/api/v1/beers/" + savedBeer.getId().toString());

        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @GetMapping(BEER_PATH)
    public Page<BeerDTO> get(@RequestParam(required = false) String name,
                             @RequestParam(required = false) BeerStyle style,
                             @RequestParam(required = false) Boolean showInventory,
                             @RequestParam(required = false) Integer pageNumber,
                             @RequestParam(required = false) Integer pageSize) {
        return beerService.get(name, style, showInventory, pageNumber, pageSize);
    }

    @GetMapping(BEER_PATH_ID)
    public BeerDTO getById(@PathVariable UUID id) {
        log.debug("Get Beer by ID - in controller");

        return beerService.getById(id).orElseThrow(NotFoundException::new);
    }

    @PutMapping(BEER_PATH_ID)
    public ResponseEntity update(@PathVariable UUID id, @Validated @RequestBody BeerDTO beerDTO) {
        if (beerService.update(id, beerDTO).isEmpty()) {
            throw new NotFoundException();
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(BEER_PATH_ID)
    public ResponseEntity patch(@PathVariable UUID id, @RequestBody BeerDTO beerDTO) {
        beerService.patch(id, beerDTO);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(BEER_PATH_ID)
    public ResponseEntity delete(@PathVariable UUID id) {
        if (!beerService.delete(id)) {
            throw new NotFoundException();
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
