package guru.springframework.spring6webmvc.repositories;

import guru.springframework.spring6webmvc.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CategoryRepository extends JpaRepository<Category, UUID> {
}
