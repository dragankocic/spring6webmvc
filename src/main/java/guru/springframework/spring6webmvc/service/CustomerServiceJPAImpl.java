package guru.springframework.spring6webmvc.service;

import guru.springframework.spring6webmvc.mappers.CustomerMapper;
import guru.springframework.spring6webmvc.model.CustomerDTO;
import guru.springframework.spring6webmvc.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@RequiredArgsConstructor
@Service
@Primary
public class CustomerServiceJPAImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;
    @Override
    public CustomerDTO save(CustomerDTO customerDTO) {
        return customerMapper.customerToCustomerDto(
                customerRepository.save(
                        customerMapper.customerDtoToCustomer(customerDTO)
                )
        );
    }

    @Override
    public List<CustomerDTO> get() {
        return customerRepository.findAll()
                .stream()
                .map(customerMapper::customerToCustomerDto)
                .toList();
    }

    @Override
    public Optional<CustomerDTO> getById(UUID id) {
        return Optional.ofNullable(
                customerMapper.customerToCustomerDto(
                        customerRepository.findById(id)
                                .orElse(null)
                ));
    }

    @Override
    public Optional<CustomerDTO> update(UUID id, CustomerDTO customerDTO) {
        var atomicReference = new AtomicReference<Optional<CustomerDTO>>();

        customerRepository.findById(id).ifPresentOrElse(foundCustomer -> {
            foundCustomer.setName(customerDTO.getName());
            atomicReference.set(
                    Optional.of(
                            customerMapper.customerToCustomerDto(
                                    customerRepository.save(foundCustomer)
                            )
                    )
            );
                },
                () -> atomicReference.set(Optional.empty())
        );

        return atomicReference.get();
    }

    @Override
    public Boolean delete(UUID id) {
        if (!customerRepository.existsById(id)) {
            return Boolean.FALSE;
        }

        customerRepository.deleteById(id);
        return Boolean.TRUE;
    }

    @Override
    public void patch(UUID id, CustomerDTO customerDTO) {

    }
}
