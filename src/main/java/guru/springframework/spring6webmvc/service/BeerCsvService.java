package guru.springframework.spring6webmvc.service;

import guru.springframework.spring6webmvc.model.BeerCSVRecord;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public interface BeerCsvService {

    List<BeerCSVRecord> convertCSV(File file);
}
