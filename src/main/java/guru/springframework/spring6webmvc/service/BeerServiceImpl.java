package guru.springframework.spring6webmvc.service;

import guru.springframework.spring6webmvc.model.BeerDTO;
import guru.springframework.spring6webmvc.model.BeerStyle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
public class BeerServiceImpl implements BeerService {

    private Map<UUID, BeerDTO> beers;

    public BeerServiceImpl() {
        beers = new HashMap<>();

        var beer1 = BeerDTO.builder()
                .id(UUID.randomUUID())
                .version(1)
                .name("Galaxy Cat")
                .style(BeerStyle.PALE_ALE)
                .upc("123456")
                .price(new BigDecimal("12.99"))
                .quantityOnHand(122)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var beer2 = BeerDTO.builder()
                .id(UUID.randomUUID())
                .version(1)
                .name("Crank")
                .style(BeerStyle.PALE_ALE)
                .upc("12356222")
                .price(new BigDecimal("11.99"))
                .quantityOnHand(392)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var beer3 = BeerDTO.builder()
                .id(UUID.randomUUID())
                .version(1)
                .name("Sunshine City")
                .style(BeerStyle.IPA)
                .upc("12356")
                .price(new BigDecimal("13.99"))
                .quantityOnHand(144)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        beers.put(beer1.getId(), beer1);
        beers.put(beer2.getId(), beer2);
        beers.put(beer3.getId(), beer3);
    }

    @Override
    public BeerDTO save(BeerDTO beerDTO) {
        BeerDTO savedBeerDTO = BeerDTO.builder()
                .id(UUID.randomUUID())
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .name(beerDTO.getName())
                .style(beerDTO.getStyle())
                .quantityOnHand(beerDTO.getQuantityOnHand())
                .upc(beerDTO.getUpc())
                .price(beerDTO.getPrice())
                .version(1)
                .build();

        beers.put(savedBeerDTO.getId(), savedBeerDTO);
        return savedBeerDTO;
    }

    @Override
    public Page<BeerDTO> get(String name, BeerStyle style, Boolean showInventory, Integer pageNumber, Integer pageSize) {
        return new PageImpl<>(new ArrayList<>(beers.values()));
    }

    @Override
    public Optional<BeerDTO> getById(UUID id) {
        log.debug("Get Beer by ID - in service. Id: " + id.toString());

        return Optional.of(beers.get(id));
    }

    @Override
    public Optional<BeerDTO> update(UUID id, BeerDTO beerDTO) {
        if (!beers.containsKey(id)) return Optional.empty();

        BeerDTO existing = beers.get(id);
        existing.setName(beerDTO.getName());
        existing.setPrice(beerDTO.getPrice());
        existing.setUpc(beerDTO.getUpc());
        existing.setQuantityOnHand(beerDTO.getQuantityOnHand());
        existing.setVersion(existing.getVersion() + 1);
        existing.setUpdateDate(LocalDateTime.now());

        return Optional.of(existing);
    }

    @Override
    public Optional<BeerDTO> patch(UUID id, BeerDTO beerDTO) {
        var existing = beers.get(id);

        if (StringUtils.hasText(beerDTO.getName())) {
            existing.setName(beerDTO.getName());
        }

        if (beerDTO.getStyle() != null) {
            existing.setStyle(beerDTO.getStyle());
        }

        if (beerDTO.getPrice() != null) {
            existing.setPrice(beerDTO.getPrice());
        }

        if (beerDTO.getQuantityOnHand() != null) {
            existing.setQuantityOnHand(beerDTO.getQuantityOnHand());
        }

        if (StringUtils.hasText(beerDTO.getUpc())) {
            existing.setUpc(beerDTO.getUpc());
        }

        return Optional.of(existing);
    }

    @Override
    public Boolean delete(UUID id) {
        if (!beers.containsKey(id)) return Boolean.FALSE;

        beers.remove(id);
        return Boolean.TRUE;
    }
}
