package guru.springframework.spring6webmvc.service;

import guru.springframework.spring6webmvc.entities.Beer;
import guru.springframework.spring6webmvc.mappers.BeerMapper;
import guru.springframework.spring6webmvc.model.BeerDTO;
import guru.springframework.spring6webmvc.model.BeerStyle;
import guru.springframework.spring6webmvc.repositories.BeerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@Service
@Primary
@RequiredArgsConstructor
public class BeerServiceJPAImpl implements BeerService {

    private final BeerRepository beerRepository;
    private final BeerMapper beerMapper;

    private static final int DEFAULT_PAGE = 0;
    private static final int DEFAULT_PAGE_SIZE = 25;
    @Override
    public BeerDTO save(BeerDTO beerDTO) {
        return beerMapper.beerToBeerDto(
                beerRepository.save(
                        beerMapper.beerDtoToBeer(beerDTO)
                )
        );
    }

    @Override
    public Page<BeerDTO> get(String name, BeerStyle style, Boolean showInventory, Integer pageNumber, Integer pageSize) {
        Page<Beer> beerPage;
        PageRequest pageRequest = buildPageRequest(pageNumber, pageSize);

        if(StringUtils.hasText(name) && style == null) {
            beerPage = getByName(name, pageRequest);
        } else if (!StringUtils.hasText(name) && style != null){
            beerPage = getByStyle(style, pageRequest);
        } else if (StringUtils.hasText(name) && style != null) {
            beerPage = getByNameAndStyle(name, style, pageRequest);
        }
        else {
            beerPage = beerRepository.findAll(pageRequest);
        }

        if (showInventory != null && !showInventory) {
            beerPage.forEach(beer -> beer.setQuantityOnHand(null));
        }



        return beerPage.map(beerMapper::beerToBeerDto);
    }

    public PageRequest buildPageRequest(Integer pageNumber, Integer pageSize) {
        int queryPageNumber;
        int queryPageSize;

        if (pageNumber != null && pageNumber > 0) {
            queryPageNumber = pageNumber - 1;
        } else {
            queryPageNumber = DEFAULT_PAGE;
        }

        if (pageSize == null) {
            queryPageSize = DEFAULT_PAGE_SIZE;
        } else {
            if (pageSize > 1000) {
                queryPageSize = 1000;
            } else {
                queryPageSize = pageSize;
            }
        }

        Sort sort = Sort.by(Sort.Order.asc("name"));

        return PageRequest.of(queryPageNumber, queryPageSize, sort);
    }

    private Page<Beer> getByNameAndStyle(String name, BeerStyle style, PageRequest pageRequest) {
        return beerRepository.findAllByNameIsLikeIgnoreCaseAndStyle("%" + name + "%", style, pageRequest);
    }


    public Page<Beer> getByName(String name, PageRequest pageRequest) {
        return beerRepository.findAllByNameIsLikeIgnoreCase("%" + name + "%", pageRequest);
    }

    private Page<Beer> getByStyle(BeerStyle style, PageRequest pageRequest) {
        return beerRepository.findAllByStyle(style, pageRequest);
    }
    @Override
    public Optional<BeerDTO> getById(UUID id) {
        return Optional.ofNullable(
                beerMapper.beerToBeerDto(
                        beerRepository.findById(id)
                                .orElse(null)
                ));
    }

    @Override
    public Optional<BeerDTO> update(UUID id, BeerDTO beerDTO) {
        var atomicReference = new AtomicReference<Optional<BeerDTO>>();

        beerRepository.findById(id).ifPresentOrElse(foundBeer -> {
                    foundBeer.setName(beerDTO.getName());
                    foundBeer.setStyle(beerDTO.getStyle());
                    foundBeer.setUpc(beerDTO.getUpc());
                    foundBeer.setPrice(beerDTO.getPrice());
                    atomicReference.set(Optional.of(beerMapper.beerToBeerDto(beerRepository.save(foundBeer))));
                },
                () -> atomicReference.set(Optional.empty())
        );

        return atomicReference.get();
    }

    @Override
    public Boolean delete(UUID id) {
        if (!beerRepository.existsById(id)) return Boolean.FALSE;

        beerRepository.deleteById(id);
        return Boolean.TRUE;
    }

    @Override
    public Optional<BeerDTO> patch(UUID id, BeerDTO beerDTO) {
        AtomicReference<Optional<BeerDTO>> atomicReference = new AtomicReference<>();

        beerRepository.findById(id).ifPresentOrElse(foundBeer -> {
            if (StringUtils.hasText(beerDTO.getName())){
                foundBeer.setName(beerDTO.getName());
            }
            if (beerDTO.getStyle() != null){
                foundBeer.setStyle(beerDTO.getStyle());
            }
            if (StringUtils.hasText(beerDTO.getUpc())){
                foundBeer.setUpc(beerDTO.getUpc());
            }
            if (beerDTO.getPrice() != null){
                foundBeer.setPrice(beerDTO.getPrice());
            }
            if (beerDTO.getQuantityOnHand() != null){
                foundBeer.setQuantityOnHand(beerDTO.getQuantityOnHand());
            }
            atomicReference.set(Optional.of(beerMapper
                    .beerToBeerDto(beerRepository.save(foundBeer))));
        }, () ->
            atomicReference.set(Optional.empty())
        );

        return atomicReference.get();
    }
}
