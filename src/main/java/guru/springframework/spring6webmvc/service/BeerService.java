package guru.springframework.spring6webmvc.service;

import guru.springframework.spring6webmvc.model.BeerDTO;
import guru.springframework.spring6webmvc.model.BeerStyle;
import org.springframework.data.domain.Page;

import java.util.Optional;
import java.util.UUID;

public interface BeerService {

    BeerDTO save(BeerDTO beerDTO);

    Page<BeerDTO> get(String name, BeerStyle style, Boolean showInventory, Integer pageNumber, Integer pageSize);

    Optional<BeerDTO> getById(UUID id);

    Optional<BeerDTO> update(UUID id, BeerDTO beerDTO);

    Boolean delete(UUID id);

    Optional<BeerDTO> patch(UUID id, BeerDTO beerDTO);
}
