package guru.springframework.spring6webmvc.service;

import guru.springframework.spring6webmvc.model.CustomerDTO;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CustomerService {

    CustomerDTO save(CustomerDTO customerDTO);
    List<CustomerDTO> get();
    Optional<CustomerDTO> getById(UUID id);
    Optional<CustomerDTO> update(UUID id, CustomerDTO customerDTO);
    Boolean delete(UUID id);
    void patch(UUID id, CustomerDTO customerDTO);
}
