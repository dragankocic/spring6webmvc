package guru.springframework.spring6webmvc.service;

import guru.springframework.spring6webmvc.model.CustomerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.*;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    private Map<UUID, CustomerDTO> customers;

    public CustomerServiceImpl() {
        customers = new HashMap<>();

        var customer1 = CustomerDTO.builder()
                .id(UUID.randomUUID())
                .name("Albert Einstein")
                .version(1)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var customer2 = CustomerDTO.builder()
                .id(UUID.randomUUID())
                .name("Isaak Newton")
                .version(1)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        var customer3 = CustomerDTO.builder()
                .id(UUID.randomUUID())
                .name("Stephen Hawkins")
                .version(1)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        customers.put(customer1.getId(), customer1);
        customers.put(customer2.getId(), customer2);
        customers.put(customer3.getId(), customer3);
    }

    @Override
    public CustomerDTO save(CustomerDTO customerDTO) {
        var savedCustomer = CustomerDTO.builder()
                .id(UUID.randomUUID())
                .version(1)
                .createdDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .name(customerDTO.getName())
                .build();

        customers.put(savedCustomer.getId(), savedCustomer);
        return savedCustomer;
    }

    @Override
    public List<CustomerDTO> get() {
        return new ArrayList<>(customers.values());
    }

    @Override
    public Optional<CustomerDTO> getById(UUID id) {
        log.debug("Get Customer by ID - in service. Id: " + id.toString());

        return Optional.of(customers.get(id));
    }

    @Override
    public Optional<CustomerDTO> update(UUID id, CustomerDTO customerDTO) {
        if (!customers.containsKey(id)) return Optional.empty();

        var existing = customers.get(id);
        existing.setName(customerDTO.getName());
        existing.setVersion(existing.getVersion() + 1);
        existing.setCreatedDate(LocalDateTime.now());
        existing.setUpdateDate(LocalDateTime.now());

        return Optional.of(existing);
    }

    @Override
    public void patch(UUID id, CustomerDTO customerDTO) {
        var existing = customers.get(id);

        if (customerDTO.getName() != null) {
            existing.setName(customerDTO.getName());
        }

        existing.setVersion(existing.getVersion() + 1);
        existing.setUpdateDate(LocalDateTime.now());
    }

    @Override
    public Boolean delete(UUID id) {
        if (!customers.containsKey(id))
            return Boolean.FALSE;

        customers.remove(id);
        return Boolean.TRUE;
    }
}
