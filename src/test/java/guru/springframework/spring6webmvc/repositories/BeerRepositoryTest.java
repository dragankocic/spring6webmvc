package guru.springframework.spring6webmvc.repositories;

import guru.springframework.spring6webmvc.bootstrap.BootstrapData;
import guru.springframework.spring6webmvc.entities.Beer;
import guru.springframework.spring6webmvc.model.BeerStyle;
import guru.springframework.spring6webmvc.service.BeerCsvServiceImpl;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@Import({BootstrapData.class, BeerCsvServiceImpl.class})
class BeerRepositoryTest {

    @Autowired
    BeerRepository beerRepository;

    @Test
    void testSaveBeer() {
        Beer savedBeer = beerRepository.save(Beer.builder()
                        .name("My Beer")
                        .style(BeerStyle.PALE_ALE)
                        .upc("234234234234")
                        .price((new BigDecimal("11.99")))
                .build());

        beerRepository.flush();

        assertThat(savedBeer).isNotNull();
        assertThat(savedBeer.getId()).isNotNull();
    }

    @Test
    void testSaveBeerNameTooLong() {
        assertThrows(ConstraintViolationException.class, () -> {
            Beer savedBeer = beerRepository.save(Beer.builder()
                    .name("1".repeat(51))
                    .style(BeerStyle.PALE_ALE)
                    .upc("234234234234")
                    .price((new BigDecimal("11.99")))
                    .build());

            beerRepository.flush();
        });
    }

    @Test
    void testGetListByName() {
        var list = beerRepository.findAllByNameIsLikeIgnoreCase("%IPA%", null);

        assertThat(list.getContent().size()).isEqualTo(336);
    }
}