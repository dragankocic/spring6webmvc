package guru.springframework.spring6webmvc.repositories;

import guru.springframework.spring6webmvc.entities.Beer;
import guru.springframework.spring6webmvc.entities.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CategoryRepositoryTest {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    BeerRepository beerRepository;

    Beer testBeer;

    @BeforeEach
    void setUp() {
        testBeer = beerRepository.findAll().getFirst();
    }

    @Transactional
    @Test
    void testAddCategory() {
        Category savedCat = categoryRepository.save(
                Category.builder()
                        .description("Ales").build());

        testBeer.addCategory(savedCat);
        var saveBeer = beerRepository.save(testBeer);

        System.out.println(saveBeer.getName());

    }

}