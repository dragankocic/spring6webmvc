package guru.springframework.spring6webmvc.controller;

import guru.springframework.spring6webmvc.entities.Customer;
import guru.springframework.spring6webmvc.mappers.CustomerMapper;
import guru.springframework.spring6webmvc.model.CustomerDTO;
import guru.springframework.spring6webmvc.repositories.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class CustomerControllerIT {

    @Autowired
    CustomerController customerController;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerMapper customerMapper;

    @Rollback
    @Transactional
    @Test
    void saveNewCustomerTest() {
        var customerDTO = CustomerDTO.builder()
                .name("New Customer")
                .build();

        var responseEntity = customerController.save(customerDTO);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.valueOf(201));
        assertThat(responseEntity.getHeaders().getLocation()).isNotNull();

        var locationUUID = responseEntity.getHeaders().getLocation().toString().split("/");
        var uuid = UUID.fromString(locationUUID[locationUUID.length - 1]);

        var customer = customerRepository.findById(uuid).get();
        assertThat(customer).isNotNull();
    }

    @Test
    void testGetById() {
        Customer customer = customerRepository.findAll().getFirst();
        CustomerDTO dto = customerController.getById(customer.getId());

        assertThat(dto).isNotNull();
    }

    @Test
    void testCustomerIdNotFound() {
        var uuid = UUID.randomUUID();

        assertThrows(NotFoundException.class, () -> {
            customerController.getById(uuid);
        });
    }

    @Test
    void testListCustomers() {
        var dtos = customerController.get();

        assertThat(dtos).hasSize(3);
    }

    @Rollback
    @Transactional
    @Test
    void testEmptyList() {
        customerRepository.deleteAll();
        var dtos = customerController.get();

        assertThat(dtos).isEmpty();
    }


    @Test
    void testUpdateNotFound() {
        var uuid = UUID.randomUUID();
        var customer = CustomerDTO.builder().build();

        assertThrows(NotFoundException.class, () ->
                customerController.update(uuid, customer));
    }

    @Rollback
    @Transactional
    @Test
    void updateExistingCustomerTest() {
        var customer = customerRepository.findAll().getFirst();
        var customerDTO = customerMapper.customerToCustomerDto(customer);
        customerDTO.setId(null);
        customerDTO.setVersion(null);
        final String customerName = "UPDATED";
        customerDTO.setName(customerName);

        var responseEntity = customerController.update(customer.getId(), customerDTO);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(204));

        var updatedCustomer = customerRepository.findById(customer.getId()).get();
        assertThat(updatedCustomer.getName()).isEqualTo(customerName);
    }

    @Rollback
    @Transactional
    @Test
    void deleteByIdFound() {
        Customer customer = customerRepository.findAll().getFirst();

        ResponseEntity responseEntity = customerController.delete(customer.getId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(204));

        assertThat(customerRepository.existsById(customer.getId())).isFalse();
    }

    @Test
    void testDeleteNotFound() {
        var uuid = UUID.randomUUID();

        assertThrows(NotFoundException.class, () ->
                customerController.delete(uuid));
    }
}